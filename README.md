## Build Image

create this structure:
```
codeigniter/
    - conf/apache.conf
    - Dockerfile
    - startScript.sh
```

Go to the codeigniter folder:
```
cd codeigniter
```

build the image:<br>
```
docker build . -t codeigniter:4.1.5
```

start the container:
```
docker container run -it --publish 80:80 --name ci4 -v /localfolder:/var/www/html codeigniter:4.1.5
```

## Installation

1. Install [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/install/) ;

2. Copy `docker-compose.yml` file to your project root path, and edit it according to your needs ;

3. From your project directory, start up your application by running:

```sh
docker-compose up -d
```

4. From your project directory, stop your application by running:

```sh
docker-compose down --volumes
```

## Contributing

Contributions are welcome!
Leave an issue on Github, or create a Pull Request.


## Licence

This work is under [MIT](LICENSE) licence.

rozaqiwt
=======
## Build Image

create this structure:
```
codeigniter/
    - conf/apache.conf
    - Dockerfile
    - startScript.sh
```

Go to the codeigniter folder:
```
cd codeigniter
```

build the image:<br>
```
docker build . -t codeigniter:4.1.5
```

start the container:
```
docker container run -it --publish 80:80 --name ci4 -v /localfolder:/var/www/html codeigniter:4.1.5
```

## Installation

1. Install [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/install/) ;

2. Copy `docker-compose.yml` file to your project root path, and edit it according to your needs ;

3. From your project directory, start up your application by running:

```sh
docker-compose up -d
```

4. From your project directory, stop your application by running:

```sh
docker-compose down --volumes
```

Hello world1123
